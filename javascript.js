"use strict";
window.addEventListener("load", loadWindow, false);


function loadWindow() {

  //event för knapptryckk
  var elementRef = document.getElementById("buttonId");
  elementRef.addEventListener("click", primTal, false);

  var cleanRef = document.getElementById("cleanButtonId");
  cleanRef.addEventListener("click", removeElements, false);

  //event vid enter click
  var textBox = document.getElementById("textID");
  textBox.addEventListener("keyup",onEnter, false);
}

function onEnter(event) {
    event.preventDefault();
    if (event.keyCode == 13) { //vid vid enter (13) så aktiversat knappens click
        document.getElementById("buttonId").click();
    }
  }

function removeElements() {

  var removeTable = document.getElementById("divId");
  if (removeTable) {
    removeTable.innerHTML = "";
  }
  var removeText = document.getElementById("antalId");
  if (removeText) {
    removeText.innerHTML = "";
  }
}

function primTal() {

  var textId = document.getElementById("textID").value; //Hämtar värdet

  //felhantering
  if (textId <= 1) {
    window.alert("Du måste ange ett nummer som är högre än 1");

  }
  else if (isNaN(textId)) {
    window.alert("Du måste ange ett nummer");

  }
  else {
    removeElements();
    var antalId = document.getElementById('antalId');
    var antalText = document.createTextNode("Primtal mellan 1 - " + textId);
    antalId.appendChild(antalText);

    var prim = [];
    //Lägger alla tal från textId i en array
    for (var i = 1; i <= textId; i++) {
      prim[i] = i;
    }

    //går igenom alla tal i arrayen
    for (var i = 2; i <= prim.length; i++) {

      //går igenom alla tal i arrayen för att kolla om det ska vara kvar eller inte
      for (var x = i; x <= prim.length; x++) {

        //om talet i arrayen är samma som varibeln i (t.ex. om 3 är samma som 3 ) så ska det vara kvar, annars fortsätter den till if-satsen
        if (prim[x] != i) {
          //om arrayens tal modulus varibeln i:s tal blir 0 så betyder dett att de går att dela med varandra, vilket gör att arraytalet inte är ett primtal

          if (prim[x] % i == 0) {
            prim.splice(x, 1); //tar bort talet
          }
        }
      }
    }
    prim.splice(1, 1); //tar bort 1 då det inte är ett primtal

    var divId = document.getElementById("divId");
    var counter = 0;
    var table = document.createElement("table");
    table.setAttribute("id", "tableId");


    //Detta skriver ut en tabell med arrayen som innehåll
    for (var i = 1; i < prim.length; i++) {
      //counter börjar om för att skapa en tabell med 5 celler
      if (counter == 0) {
        var tr = document.createElement("tr");
        var textNode = document.createTextNode(prim[i]);
        var td = document.createElement("td");
        td.appendChild(textNode);
        tr.appendChild(td);
        table.appendChild(tr);
        counter++;

      } else {
          var textNode = document.createTextNode(prim[i]);
          var td = document.createElement("td");
          td.appendChild(textNode);
          tr.appendChild(td);
          counter++;
          if (counter == 5) {
            counter = 0;
          }
      }
      divId.appendChild(table);
    }
  }
}
